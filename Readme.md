# Instructions To Perfrom Automation Testing on Django Login

## Getting Started

These instructions will get you how to run this Project on your local machine.

### Introduction

In this project, we will perform Automation testing on Django login using Selenium and Django test runner.

### Requirements

* Clone this project from bitbucket using the below command.
  * **$ git clone git@bitbucket.org:sai_kiran_nalamothu/django_login_testing.git**
* Create a virtual environment in your system and install the dependency libraries.
* The required libraries are listed in **requirements.txt** text file.

### Execution of project

* Install chrome driver by using below command in linux terminal.
  * **$ sudo apt install chromium-chromedriver**
* Install virtual environment by using the below command.
  * **$ sudo apt install python3-venv**
* Create virtual environment by using the below command.
  * **$ python3 -m venv env_name**
* Activate the virtual environment by using the below command.
  * **$ source env_name/bin/activate**
* Locate to the file location of cloned git repository.
* Install the dependency libraries by using below command.
  * **$ pip3 install -r requirements.txt**

### step-1 : Migrations

* Open terminal and perform the following commands.
  * **$ python3 manage.py migrate**
* It performs the migrations.

### step-2 : Loading user login credentials to database

* Run the following command in the terminal.
* **$ python3 manage.py loaddata users**

### step-3 : Run Deployment server

* Execute the following command in the terminal.
  * **$ python3 manage.py runserver**

### step-4 : Automation testing using selenium

* Open new tab in the terminal and execute following commands.
  * **$ source env_name/bin/activate**
  * **$ cd tests/selenium**
  * **$ behave main.feature**

### step-5 : Automation testing using Django test runner

* Execute the following commands in the termincal
  * **$ cd ../simple**
  * **$ python3 ../../manage.py behave main.feature**

*ThankYou!*

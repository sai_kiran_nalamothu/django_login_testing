Feature: Testing Django login using selenium

	Scenario: Admin Login with valid credentials
		Given Open Django login page
		And User enters "admin" as username and "admin" as password
		When User clicks on admin page login button
		Then Logout option should be displayed and logout the user

	Scenario: Admin login with invalid credentials
		Given Open Django login page
		And User enters "admin" as username and "password" as password
		When User clicks on admin page login button
		Then "Please enter the correct username and password for a staff account." should be displayed as error message

	Scenario: Staff Login with valid credentials
		Given Open Django login page
		And User enters "staff" as username and "staff" as password
		When User clicks on admin page login button
		Then Logout option should be displayed and logout the user

	Scenario: Staff login with invalid credentials
		Given Open Django login page
		And User enters "staff" as username and "password" as password
		When User clicks on admin page login button
		Then "Please enter the correct username and password for a staff account." should be displayed as error message

	Scenario: Non staff Login with valid credentials in Django login
		Given Open Django login page
		And User enters "user" as username and "user" as password
		When User clicks on admin page login button
		Then "Please enter the correct username and password for a staff account." should be displayed as error message

	Scenario: Non staff login with valid credentials in user login
		Given Open custom login page
		And User enters "user" as username and "user" as password
		When User clicks on custom page login button
		Then "Login successful" should be displayed

	Scenario: Non staff login with invalid credentials in user login
		Given Open custom login page
		And User enters "user" as username and "password" as password
		When User clicks on custom page login button
		Then "Username/Password is wrong" should be displayed

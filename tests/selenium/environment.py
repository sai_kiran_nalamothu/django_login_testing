from selenium import webdriver

def before_all(context):
	context.root_url = "http://127.0.0.1:8000"

def before_scenario(context, feature):
	context.browser = webdriver.Chrome()

def after_scenario(context, feature):
	context.browser.close()

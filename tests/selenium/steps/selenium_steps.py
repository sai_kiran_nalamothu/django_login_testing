from selenium.common.exceptions import NoSuchElementException


@given('Open Django login page')
def django_login_page(context):
    context.browser.get(context.root_url + '/admin')


@given('User enters "{username}" as username and "{password}" as password')
def fill_login_form(context, username, password):
    try:
        uname_ele = context.browser.find_element_by_name("username")
        uname_ele.send_keys(username)
        pwd_ele = context.browser.find_element_by_name("password")
        pwd_ele.send_keys(password)
    except NoSuchElementException:
        assert False, 'Element not found'


@when('User clicks on {user_type} page login button')
def submit_login_form(context, user_type):
    try:
        if(user_type == 'admin'):
            path = '//*[@id="login-form"]/div[3]/input'
        else:
            path = '/html/body/form/input[4]'

        login_button = context.browser.find_element_by_xpath(path)
        login_button.click()
    except NoSuchElementException:
        assert False, 'Element not found'


@then('Logout option should be displayed and logout the user')
def django_login_success(context):
    try:
        logout_option = context.browser.find_element_by_xpath('//*[@id="user-tools"]/a[3]')
        assert logout_option.is_displayed()
        logout_option.click()
    except NoSuchElementException:
        assert False, 'Element not found'


@then('"{error_message}" should be displayed as error message')
def django_login_failure(context, error_message):
    try:
        element = context.browser.find_element_by_xpath('//*[@id="content"]/p')
        assert error_message in element.text
    except NoSuchElementException:
        assert False, 'Element not found'


@given('Open custom login page')
def custom_login_page(context):
    context.browser.get(context.root_url + '/login')


@then('"{message}" should be displayed')
def custom_login_status(context, message):
    try:
        element = context.browser.find_element_by_name('message')
        assert element.text == message
    except NoSuchElementException:
        assert False, 'Element not found'

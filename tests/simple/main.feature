Feature: Django login automation test using Django test runner

	Scenario: Admin login with valid credentials
		Given "Admin" user enters "admin" as username and "admin" as password is "valid"
		Then Response should be "True"
		And Logout the user

	Scenario: Admin login with invalid credentials
		Given "Admin" user enters "admin" as username and "password" as password is "invalid"
		Then Response should be "False"

	Scenario: Staff Login with valid credentials
		Given "Staff" user enters "staff" as username and "staff" as password is "valid"
		Then Response should be "True"
		And Logout the user

	Scenario: Staff login with invalid credentials
		Given "Staff" user enters "staff" as username and "password" as password is "invalid"
		Then Response should be "False"

	Scenario: Non staff Login with valid credentials in Django login
		Given "Non staff" user enters "user" as username and "user" as password is "invalid"
		Then Response should be "False"
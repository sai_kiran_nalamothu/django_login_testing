from django.contrib.auth.models import User
from django.test import Client


@given(u'"{user_type}" user enters "{username}" as username and "{password}" as password is "{login_status}"')
def login_user(context, user_type, username, password, login_status):
    context.client, user = Client(), User(username=username)
    user.set_password(password)
    
    if(user_type=='Admin'):
        user.is_staff = True
        user.is_superuser = True
    elif(user_type=='Staff'):
        user.is_staff = True
    user.save()

    if(login_status == 'invalid'):
        password = 'wrong'
    
    context.resp = context.client.login(username=username,
                                            password=password)


@then('Response should be "{boolean_value}"')
def login_status(context, boolean_value):
    assert str(context.resp) == boolean_value


@then('Logout the user')
def logout_user(context):
    context.client.logout()

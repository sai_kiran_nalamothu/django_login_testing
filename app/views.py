from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password


def login(request):
    if(request.method == 'POST'):
        username = request.POST['username']
        password = request.POST['password']
        user = User.objects.filter(username=username).values('username', 'password', 'is_staff', 'is_superuser').first()
        if(
            user and
            check_password(password, user['password']) and
            not user['is_staff'] and
            not user['is_superuser']
            ):
            message = 'Login successful'
        else:
            message = 'Username/Password is wrong'       
    else:
        message=''
    return render(request, 'login.html', {'message': message})
